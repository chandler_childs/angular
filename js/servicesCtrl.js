app.service('myService', function(){
    this.hexify = function(x) {
        return x.toString(16);
    };
});

app.controller('servicesCtrl', function($scope, $location, $timeout, $interval, myService){
    $scope.myURL = $location.absUrl();
    
    $timeout(function(){
        $scope.timeMessage = "You have been here for 2 seconds"
    }, 2000);
    $timeout(function(){
        $scope.timeMessage = "You have been here for 5 seconds"
    }, 5000);
    $timeout(function(){
        $scope.timeMessage = "You have been here for 10 seconds"
    }, 10000);
    $timeout(function(){
        $scope.timeMessage = "You have been here for 30 seconds"
    }, 30000);
    $timeout(function(){
        $scope.timeMessage = "You have been here for 1 minute"
    }, 60000);
    
    $scope.theTime = new Date().toLocaleTimeString();
    $interval(function(){
        $scope.theTime = new Date().toLocaleTimeString();
    }, 1000);
    
    $scope.hex = myService.hexify(255);
    
});

app.filter('hexFormat', ['myService', function(myService){
    return function(x){
      return myService.hexify(x);  
    };
}]);