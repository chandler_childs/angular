app.controller("dataCtrl", function($scope){
    $scope.dataTest = " controller working";
    $scope.roomates = [
        {first:'Joe', last:'Bryce'},
        {first:'Jarrett', last:'Iverson'},
        {first:'Ryan', last:'Hernandez'},
        {first:'Ioua', last:'Lagazo'},
        {first:'Nate', last:'Gardner'},
        {first:'Matt', last:'Hoskison'},
        {first:'Jordan', last:'Jensen'},
        {first:'Eric', last:'Thornley'},
        {first:'Andy', last:'Mokler'},
        {first:'Rheese', last:'Jackson'},
        {first:'Andrew', last: 'Germaine'},
        {first:'Brad', last: 'Benson'},
        {first:'Sam', last:'Heinzen'},
        {first:'Beck', last: 'Mayberry'},
        {first:'Ben', last: 'Johnson'},
        {first:'Quentin', last:'Ellis'}
    ];
    $scope.books = [
        {title: "A better way to lean javascript", price: 19},
        {title: "Differential Equations", price: 199.95234}
    ];
    
    $scope.filterInput = "";
    $scope.orderInput = function(input){
        $scope.dataTest = input + " clicked"
        $scope.selectedOrder = input;
    };
    
});

