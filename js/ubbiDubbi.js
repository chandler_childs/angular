app.filter('myFormat', function() {
    return function(x) {
        var i, c, txt = "";
        x = x.split("")
        for (i = 0; i < x.length; i++) {
            c = x[i];
            if (i % 2 == 0) {
                c = c.toUpperCase();
            }
            txt += c;
        }
        return txt;
    };
});

app.filter("ubbiDubbi", function(){
    return function(x){
        var result = "";
        x = x.split("");
        for(var i = 0; i < x.length; i++){
            var c = x[i];
            if(c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'){
                c = "ub" + c;
            }
            result += c;
        }
        return result;
    };
});